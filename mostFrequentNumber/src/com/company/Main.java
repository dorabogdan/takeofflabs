package com.company;

import java.util.*;

public class Main {

    static void mostFrequentNumber(int[] numbers, int n, int k) {

        Map<Integer, Integer> mp = new HashMap<>(); //store element-frequency pair

        for (int i = 0; i < n; i++) {
            mp.put(numbers[i], mp.getOrDefault(numbers[i], 0) + 1); //update the array for every element
        }

        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(mp.entrySet());

        //sort the list in decreasing order of frequency
        Collections.sort(list, (o1, o2) -> {
            if (o1.getValue() == o2.getValue())
                return o2.getKey() - o1.getKey();
            else
                return o2.getValue() - o1.getValue();
        });

        for (int i = 0; i < k; i++)
            System.out.print(list.get(i).getKey() + " ");
    }

    public static void main(String[] args) {
        int numbers[] = {6, 5, 2, 6, 6, 2, 1, 7, 3, 3, 3};
        int n = numbers.length;
        int k = 3;
        mostFrequentNumber(numbers, n, k);
    }
}
