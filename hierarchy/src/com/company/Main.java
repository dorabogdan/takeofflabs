package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Main {
    //find all employees who reports to a manager
    private static List<String> findAllReportingEmployees(String manager,
                                                          Map<String, List<String>> managerToEmployee,
                                                          Map<String, List<String>> result) {
        if (result.containsKey(manager)) {
            return result.get(manager);
        }

        List<String> managerEmployees = managerToEmployee.get(manager);

        if (managerEmployees != null) {
            for (String reportee : new ArrayList<>(managerEmployees)) {
                // find all employees reporting to the current employee
                List<String> employees = findAllReportingEmployees(reportee,
                        managerToEmployee, result);

                // move employees to the current manager
                if (employees != null) {
                    managerEmployees.addAll(employees);
                }
            }
        }

        // save the result
        result.put(manager, managerEmployees);
        return managerEmployees;
    }

    // find all employees who reports to a manager
    public static Map<String, List<String>> findEmployees(Map<String, String> employeeToManager) {
        // store manager to employee mappings in a new map
        // List<String> is used since a manager can have several employees mapped
        Map<String, List<String>> managerToEmployee = new HashMap<>();

        for (var entry : employeeToManager.entrySet()) {
            String employee = entry.getKey();
            String manager = entry.getValue();

            if (!employee.equals(manager) && !employee.equals("Unavailable")) {
                managerToEmployee.putIfAbsent(manager, new ArrayList<>());
                managerToEmployee.get(manager).add(employee);
            }
        }

        Map<String, List<String>> result = new HashMap<>();

        // find all reporting employees for every manager
        for (var entry : employeeToManager.entrySet()) {
            if (!entry.getKey().equals("Unavailable")) {
                findAllReportingEmployees(entry.getKey(), managerToEmployee, result);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        // construct a map of employee to manager mappings
        Map<String, String> employeeToManager = new HashMap<>();

        employeeToManager.put("John", "John");
        employeeToManager.put("Jasmine", "John");
        employeeToManager.put("Jay", "Jasmine");
        employeeToManager.put("Unavailable", "Jasmine");
        employeeToManager.put("Jack", "Unavailable");
        employeeToManager.put("Jeremy", "Unavailable");
        employeeToManager.put("Johanna", "John");

        System.out.println("John -> " + findEmployees(employeeToManager).get("John"));

    }
}